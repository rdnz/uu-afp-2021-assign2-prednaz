module Teletype where

data Teletype a =
  Get (Char -> Teletype a) |
  Put Char (Teletype a) |
  Return a

instance Functor Teletype where
  fmap = undefined

instance Applicative Teletype where
  pure = undefined
  (<*>) = undefined

instance Monad Teletype where
  (>>=) = undefined

instance Foldable Teletype where
  foldMap = undefined

instance Traversable Teletype where
  sequenceA = undefined
