module RoseTree where

data RoseTree a = RoseNode a [RoseTree a] | RoseLeaf

instance Functor RoseTree where
  fmap = undefined

instance Applicative RoseTree where
  pure = undefined
  (<*>) = undefined

instance Monad RoseTree where
  (>>=) = undefined

instance Foldable RoseTree where
  foldMap = undefined

instance Traversable RoseTree where
  sequenceA = undefined
