module Tree where

data Tree a = Leaf a | Node (Tree a) (Tree a)

instance Functor Tree where
  fmap = undefined

instance Applicative Tree where
  pure = undefined
  (<*>) = undefined

instance Monad Tree where
  (>>=) = undefined

instance Foldable Tree where
  foldMap = undefined

instance Traversable Tree where
  sequenceA = undefined
