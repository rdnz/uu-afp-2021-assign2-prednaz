# uu-afp-starting-point

## requirements
install `cabal-install` and GHC 8.10.7, for example via [GHCup](https://www.haskell.org/ghcup/).
you can then run the test suite via `cabal run tests` or open a REPL via `cabal repl`.

## contact
feel free to contact me, Philipp Zander, on microsoft teams.
