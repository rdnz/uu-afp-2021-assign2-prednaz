{-# language
  DeriveGeneric,
  DerivingVia,
  StandaloneDeriving,
  TypeApplications
#-}

{-# options_ghc
  -Wno-orphans
#-}

module Main where

import Tree (Tree (Leaf, Node))
import RoseTree (RoseTree (RoseNode, RoseLeaf))
import Teletype (Teletype (Get, Put, Return))

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Classes.Base
import Test.Hspec.QuickCheck (modifyMaxSuccess)
import Text.Show.Functions ()
import Data.Coerce (coerce)
import Data.Functor.Identity (Identity (Identity), runIdentity)
import Data.Char (ord)
import Data.Proxy (Proxy (Proxy))
import Data.Foldable (traverse_)
import GHC.Generics (Generic)

-- remove duplicate instance declarations if necessary
deriving instance (Eq a) => Eq (Tree a)
deriving instance (Show a) => Show (Tree a)
deriving instance (Eq a) => Eq (RoseTree a)
deriving instance (Show a) => Show (RoseTree a)
deriving instance (Show a) => Show (Teletype a)

instance (Arbitrary a) => Arbitrary (Tree a) where
  arbitrary =
    sized $
      \n ->
        if n > 0
        then
          oneof
            [
              Leaf <$> arbitrary,
              scale (`div` 2) $ Node <$> arbitrary <*> arbitrary
            ]
        else
          Leaf <$> arbitrary
  shrink = genericShrink

instance (Arbitrary a) => Arbitrary (RoseTree a) where
  arbitrary =
    sized $
      \n ->
        if n > 0
        then
          oneof
            [
              pure RoseLeaf,
              do
                size <- getSize
                subtreeCount <- chooseInt (0, size)
                RoseNode
                  <$> arbitrary
                  <*>
                    (
                      (take subtreeCount)
                      <$>
                      (scale (`div` subtreeCount) arbitrary)
                    )
            ]
        else
          oneof [pure RoseLeaf, flip RoseNode [] <$> arbitrary]
  shrink RoseLeaf = []
  shrink node = RoseLeaf : genericShrink node

instance (Arbitrary a) => Arbitrary (Teletype a) where
  arbitrary =
    sized $
      \n ->
      frequency
        [
          (n, fmap Get $ fmap applyFun $ arbitrary),
          (n, Put <$> arbitrary <*> arbitrary),
          (1, Return <$> arbitrary)
        ]
  shrink = genericShrink

instance (Eq a) => Eq (Teletype a) where
  Get f == Get g = f 'a' == g 'a'
  Put char0 teletype0 == Put char1 teletype1 =
    (char0, teletype0) == (char1, teletype1)
  Return a == Return b = a == b
  _ == _ = False

newtype TeletypeWithoutGet a =
  TeletypeWithoutGet (Teletype a)
  deriving (Eq, Show) via Teletype a
  deriving (Functor, Foldable) via Teletype

fromTeletypeWithoutGet :: TeletypeWithoutGet a -> Teletype a
fromTeletypeWithoutGet = coerce

instance Traversable TeletypeWithoutGet where
  sequenceA (TeletypeWithoutGet teletype) =
    TeletypeWithoutGet <$> sequenceA teletype

instance (Arbitrary a) => Arbitrary (TeletypeWithoutGet a) where
  arbitrary =
    fmap TeletypeWithoutGet $
    sized $
    \n ->
      frequency
        [
          (n, Put <$> arbitrary <*> fmap fromTeletypeWithoutGet arbitrary),
          (1, Return <$> arbitrary)
        ]
  shrink (TeletypeWithoutGet teletype) = coerce (shrink teletype)

main :: IO ()
main = hspec $ do
  describe "Tree" $
    propertyTestLaws (functorLaws (Proxy @Tree)) *>
    propertyTestLaws (applicativeLaws (Proxy @Tree)) *>
    propertyTestLaws (monadLaws (Proxy @Tree)) *>
    propertyTestLaws (foldableLaws (Proxy @Tree)) *>
    propertyTestLaws (traversableLaws (Proxy @Tree))
  describe "RoseTree" $
    propertyTestLaws (functorLaws (Proxy @RoseTree)) *>
    propertyTestLaws (applicativeLaws (Proxy @RoseTree)) *>
    propertyTestLaws (monadLaws (Proxy @RoseTree)) *>
    propertyTestLaws (foldableLaws (Proxy @RoseTree)) *>
    propertyTestLaws (traversableLaws (Proxy @RoseTree))
  modifyMaxSuccess (const 50) $ do
    describe "Teletype without Get" $
      propertyTestLaws (foldableLaws (Proxy @TeletypeWithoutGet)) *>
      propertyTestLaws (traversableLaws (Proxy @TeletypeWithoutGet))
    describe "Teletype" $
      propertyTestLaws (functorLaws (Proxy @Teletype)) *>
      propertyTestLaws (applicativeLaws (Proxy @Teletype)) *>
      propertyTestLaws (monadLaws (Proxy @Teletype)) *>
      it "Traversable Identity" (
        let teletype = Get (Return . ord)
        in
          runIdentity (traverse Identity teletype)
          `shouldSatisfy`
          preciselyEquals teletype
      )

preciselyEquals :: (Eq a) => Teletype a -> Teletype a -> Bool
preciselyEquals (Get f) (Get g) =
  and $ zipWith preciselyEquals (f <$> [minBound ..]) (g <$> [minBound ..])
preciselyEquals (Put char0 teletype0) (Put char1 teletype1) =
  char0 == char1 && preciselyEquals teletype0 teletype1
preciselyEquals (Return a) (Return b) = a == b
preciselyEquals _ _ = False

propertyTestLaws :: Laws -> SpecWith ()
propertyTestLaws (Laws className properties) =
  describe className $
  traverse_ (\(name, p) -> it name (property p)) $
  properties 

deriving instance Generic (Tree a)
deriving instance Generic (RoseTree a)
deriving instance Generic (Teletype a)
