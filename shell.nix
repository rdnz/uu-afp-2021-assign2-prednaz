with import <nixpkgs> {};
mkShell {
  packages =
    (with haskell.packages.ghc8107;
      [
        ghc
        cabal-install
        cabal-fmt
      ]
    );
}
